(function() {
    var app = angular.module('taskManager');

    app.decorator('TaskRepository', function ($delegate) {
        var oldsave = $delegate.save;

        // I wanna change save method()
        $delegate.save = function(task) {
            console.log('Special logging for the task ' + task.name);
            oldsave(task);
        }

        return $delegate;

    })
}())