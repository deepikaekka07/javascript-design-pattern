
/* true inheritance - sub classing */

var Task = function (name) {
    this.name = name;
    this.completed = false;
}

Task.prototype.complete = function() {
    console.log('completing task: '+ this.name);
    this.completed = true;
}

Task.prototype.save = function() {
    console.log('saving Task ' + this.name);
}

var myTask = new Task('Legacy task');
myTask.complete();
myTask.save();

var UrgentTask = function(name, priority) {
    Task.call(this, name); // using like calling super()
    this.priority = priority;
}

/*ut.complete(); // but we dont have everything. cause we cannot access task prototype
*/

/**
 * UrgentTask.prototype = Task.prototype;
 * // this should not be done. Cause in this way, new object will point to the passed object.
 * // if we do any change here. it will affect prototype of passed object.
 */

UrgentTask.prototype = Object.create(Task.prototype); 
//what happened here? think !!
// We generally think this as a copy of passed object
//but no, its not a copy
//We didn't copy, we linked newObject to passedObject.
//passedObject becomes a prototype of newObject. To know what is inside the prototype of an object, you can use proto.
// this is prototypal inheritance
UrgentTask.prototype.notify = function() {
    console.log('notifying important people');
}

UrgentTask.prototype.save = function () {
    this.notify();
    Task.prototype.save.call(this);
}

var ut = new UrgentTask("This is Urgent", 1);
console.log(ut);
ut.complete();
ut.save();
console.log(ut);