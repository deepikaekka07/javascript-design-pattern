var Task = function (name) {
    this.name = name;
    this.completed = false;
}

Task.prototype.complete = function() {
    console.log('completing task: '+ this.name);
    this.completed = true;
}

Task.prototype.save = function() {
    console.log('saving Task ' + this.name);
}

var myTask = new Task('Legacy task');
myTask.complete();
myTask.save();

var urgentTask = new Task('Urgent task');
urgentTask.priority = 2;

urgentTask.notify = function () {
    console.log('notifying important people');
}

urgentTask.complete();
urgentTask.save = function () {
    this.notify();
    Task.prototype.save.call(this); //binding this object to the save function
}

urgentTask.save();

/*
We we impleemnted urgentTask.save, we have decorated urgentTask with a new save and 
inside we are notifying important people and then again caling prototype save function usingcall

 */

 /*
 what if we want more than one urgent task , we need to again wrte save function for it. 
 which is not a good thing. We want here is sub-classing but this is javscript, 
 so let it call sub-objecting
 */





