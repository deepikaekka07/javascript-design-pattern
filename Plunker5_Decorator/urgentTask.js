(function() {
    var app = angular.module('taskManager');

    app.factory('UrgentTask', function( Task) {
        var UrgentTask = function(data) {
            Task.call(this, data); // using like calling super()
            this.priority = data.priority;
        }

     UrgentTask.prototype = Object.create(Task.prototype);
      // We generally think this as a copy of passed object
      // but no, its not a copy
      // We didn't copy, we linked newObject to passedObject.
      // passedObject becomes a prototype of newObject. To know what is inside the prototype of an object, you can use proto.
      // this is prototypal inheritance
      
        UrgentTask.prototype.notify = function() {
            console.log('notifying important people');
        }

        UrgentTask.prototype.save = function () {
            this.notify();
            Task.prototype.save.call(this);
        }

        return UrgentTask;
    });
}())

/*
Urgent.complete() will call the task prototype complete() method
*/