var repoFactory = function() {

    this.getRepo = function(repoType) {
        if (repoType === 'task') {
            if(this.taskRepo){
                console.log("Retrieving from cache");
                return this.taskRepo;
            }else{
                this.taskRepo = require('./taskRepo')();
                return this.taskRepo;
            }
        }
        if (repoType === 'project') {
            var projectRepo = require('./projectRepo')();
            return projectRepo;
        }
        if (repoType === 'user') {
            var userRepo = require('./userRepo')();
            return userRepo;
        }

    }
};

module.exports =  new repoFactory;