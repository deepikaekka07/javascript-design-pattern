//responsible for db call

var projectRepo = function(){

    var db = {};

    var get = function(id) {
        console.log('Getting project: ' + id);
        return {
            name: 'project ' + id,
        };
    };

    return {
        get: get
    }
}

module.exports = projectRepo;
