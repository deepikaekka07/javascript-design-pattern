//responsible for db call

var repo = function(){

    var db = {};

    var get = function(id) {
        console.log('Getting task: ' + id);
        return {
            name: 'data from db' + id,
        };
    };
    var save2 =  function(task) {
        console.log('saving '+ task.name + ' to the db');
    };

    return {
        get: get,
        save: save2,
    }
}

module.exports = repo;
