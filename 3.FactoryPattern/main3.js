var Task = require('./task');
var repoFactory = require('./repoFactoryWithCache');

var task1 = new Task(repoFactory.getRepo('task').get(1));
var project = repoFactory.getRepo('project').get(1)
var user = repoFactory.getRepo('user').get(1);




task1.user = user;
task1.project = project;

var task2 = new Task(repoFactory.getRepo('task').get(2));
project = repoFactory.getRepo('project').get(2)
user = repoFactory.getRepo('user').get(2);


task2.user = user;
task2.project = project;


console.log(task1);
task1.save();

console.log(task2);
task2.save();
