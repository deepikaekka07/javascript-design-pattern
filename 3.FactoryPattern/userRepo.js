//responsible for creating a user object

var userRepo = function(){

    var db = {};

    var get = function(id) {
        console.log('Getting user: ' + id);
        return {
            name: 'user ' + id,
        };
    };

    return {
        get: get
    }
}

module.exports = userRepo;
