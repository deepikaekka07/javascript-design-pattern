var Task = require('./task');

var task1 = new Task("Task 1");
var task2 = new Task("Task 2");
var task3 = new Task("Task 3");
var task4 = new Task("Task 4");

task1.complete();
task2.save();
task3.save();
task4.save();