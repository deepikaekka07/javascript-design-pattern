// (function(){
// var app = angular.module('taskManager');

// app.factory

// }());



/* // 2.------------------ Demo-contructor node--------------------------
var Task = function(name) {
    this.name = name;
    this.completed = false;


}

Task.prototype.complete = function() {
    console.log('completing task ' + this.name);
    this.completed = true;
} 


Task.prototype.save = function() {
    console.log('saving task ' + this.name);
}

// var task1 = new Task("Task 1"); //moving the statement\ to main.js
// var task2 = new Task("Task 2");
// var task3 = new Task("Task 3");
// var task4 = new Task("Task 4");

// task1.complete();
// task2.save();
// task3.save();
// task4.save();


 
module.exports = Task;

*/

/*// 1. -------------------- constructor pattern ---------------------
var Task = function(name) {
    this.name = name;
    this.completed = false;

    this.completed = function() {
        this.completed = true;
    }

    this.save = function() {
        console.log('saving task ' + this.name);
    }
}

var task1 = new Task("Task 1");
var task2 = new Task("Task 2");
var task3 = new Task("Task 3");
var task4 = new Task("Task 4");

task1.completed();
task2.save();
*/