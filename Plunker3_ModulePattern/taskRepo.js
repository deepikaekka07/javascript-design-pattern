(function(){

    var app = angular.module('taskManager');

    
var taskRepo = function($http){

    var db = {};

    var get = function(id) {
        console.log('Getting task: ' + id);
        return {
            name: 'task ' + id 
        };
    };
    var save =  function(task) {
        console.log('saving '+ task.name + ' to the db');
    };

    return {
        get: get,
        save: save
    }
}

app.service('TaskRepository', taskRepo);

}());