(function(){
 var app = angular.module('taskManager', []);
 
 var taskController = function(){
     var ctrl = this;
     ctrl.tasks = [{name:'task1'},{name:'task2'}, {name: 'task3'}];

     this.complete = function(item) {
         console.log("completing the task "+ ctrl.tasks[item].name);
         ctrl.tasks[item].completed = true;
     }
 }

 app.controller('taskCtrl', taskController);
}());