/* In angular , services are always singleton.  That is the way angular works*/

(function(){
    var app = angular.module('taskManager', []);
    
    var taskController = function(Task, TaskService, TaskServiceWrapper){
        var ctrl = this;
        
        ctrl.tasks = [new Task({
            name: 'myTask',
            priority: 1,
            project: 'Courses',
            user: 'Jon',
            completed: false
        })];

        ctrl.completeTask = function(i){
            var myTask = ctrl.tasks[i];
            TaskServiceWrapper.completeAndNotify(myTask);
            console.log(myTask);
        }
        
    }
   
    app.controller('taskCtrl', taskController);
   }());