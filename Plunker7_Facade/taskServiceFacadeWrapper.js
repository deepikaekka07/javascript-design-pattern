(function() {
    var app = angular.module('taskManager');
    var taskServiceFacade = function(TaskService) {
        var completeAndNotify = function(task) {
            TaskService.complete(task);
            if(task.completed == true) {
                TaskService.setCompleteDate(task);
                TaskService.notifyCompletion(task, task.user);
                TaskService.save(task);
            }
        }

        return {
            completeAndNotify: completeAndNotify
        };
    };

    app.service('TaskServiceWrapper', taskServiceFacade);
}())