(function(){

    var app = angular.module('taskManager');

    
    var taskService = function($http){

        return {
            complete: function(task) {
                task.completed = true;
                console.log('completing task: '+ task.name);
            },
            setCompleteDate:function (task) {
                task.completedDate = new Date();
                console.log(task.name + ' completed on ' + task.completedDate);
            },
            notifyCompletion: function (task, user) {
                console.log('notifying ' + user + ' of the completion of ' + task.name);
            },
            save: function (task) {
                console.log('saving Task' + task.name);
            }
        }
    
    }

    
app.service('TaskService', taskService);

}());