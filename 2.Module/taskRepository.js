//responsible for db call

var repo = function(){

    var db = {};

    var get = function(id) {
        console.log('Getting task: ' + id);
        return {
            name: 'data from db'
        };
    };
    var save =  function(task) {
        console.log('saving '+ task.name + ' to the db');
    };

    return {
        get: get,
        save: save
    }
}

module.exports = repo();

// in module pattern we return object literal 